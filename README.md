Step to include ulfius into Buildroot.
1. Download latest Buildroot.
   https://buildroot.org/download.html

2.  Copy ulfius folder into 'Buildroot/package/' path

3.  add ulfius to buildroot config file
    i.e  Buildroot/package/Config.in 
    I placed in @ menu "Networking"
	source "package/libhttpparser/Config.in"
+	source "package/ulfius/Config.in"
	source "package/libidn/Config.in"
4. Do menuconfig for the buildroot,select ulfius option under networking menu to build ulfius library.
