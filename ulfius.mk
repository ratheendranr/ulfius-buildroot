################################################################################
#
# ulfius 
#
################################################################################

ULFIUS_VERSION = ad761d6e247159265559ec1491d068f683ea4d15
ULFIUS_SITE = $(call github,babelouest,ulfius,$(ULFIUS_VERSION))
ULFIUS_LICENSE = LGPLv2.1+
ULFIUS_LICENSE_FILES = LICENSE
ULFIUS_INSTALL_STAGING = YES
ULFIUS_DEPENDENCIES = libmicrohttpd host-pkgconf
ULFIUS_CONF_OPTS = -DWITH_JOURNALD=off -DWITH_YDER=off -DWITH_JANSSON=off -DBUILD_DOCS=off -DBUILD_EXAMPLE=off


$(eval $(cmake-package))

